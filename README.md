# go_utils

#### 介绍
自用工具库

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1. **checkErr.go**

   1. CheckErr() //检测到错误退出

      ```go
      CheckErr(err)
      ```

   2. CheckErrPrint() //检测到错误打印

      ```go
      CheckErrPrint(err)
      ```

2. **FileTools.go**

   1. DownFile() //下载文件，返回[]byte

      ```go
      FileInfo := DownFile(url)
      ```

   2.  GetFileList() //获取文件列表

       ```go
       filelist :=new (FileListStruct)
       filelist.GetFileList(url)
       fmt.Println("filelist:",filelist)
       ```

   3.  CopyFile() //复制文件到指定位置

       ```go
       CopyFile(srcName,dstName)
       ```

   4.  read()读取配置文件

       ```go
       myConfig := new(cf.Config)
       myConfig.InitConfig("config.ini")
       mysqladdr := myConfig.Read("default", "ipaddr")
       fmt.Printf("%v", myConfig.Mymap)
       for v, a := range myConfig.Mymap {
       	fmt.Println(v, a)
       }
       ```

   5.  PathExists() //判断文件夹是否存在

       ```go
       PathExists(pathUrl,true) //如果为true，如果不存在文件夹则会创建一个
       ```

       

3. xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1. 作为go mod 使用

   1. 发布

      ```bash
      #发布版本号
      git tag v0.0.1
      #推送版本号
      git push origin v0.0.1
      
      #更多的版本号累积即可
      git tag v0.0.2
      git push origin v0.0.2
      ```

   2.  使用

       ```bash
       #下载指定版本，不指定版本号时会下载默认下载最新版本, 并记录到 go.mod 文件中
       go get gitee.com/taadis/uuid v0.0.1
       ```

       

   go mod 使用方法来源：https://www.cnblogs.com/taadis/p/12132809.html