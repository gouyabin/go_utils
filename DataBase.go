package utils

import (
	"database/sql"
	"reflect"
)

const (
	SqlField = "SqlField" //自创建数据库字段名称标记
	SqlType  = "SqlType"  //自创建数据库字段类型标记
)

//根据结构类型，以及结构后面的tag 创建数据库结构
func createTablesStruct(td interface{}, tablename string, CONN *sql.DB) {
	t := reflect.TypeOf(td)

	createTableCmd := "CREATE TABLE IF NOT EXISTS "
	tableFieldStr := ""
	for i := 0; i < t.NumField(); i++ {
		if i != 0 {
			tableFieldStr = tableFieldStr + ","
		}
		field := t.Field(i)
		typeTag := field.Tag.Get(SqlType)
		tableTag := field.Tag.Get(SqlField)
		tableFieldStr = tableFieldStr + tableTag + " " + typeTag
		// fmt.Printf("%d. %v(%v), tag:'%v'\n", i+1, field.Name, field.Type.Name(), typeTag)
	}
	createTableCmd = createTableCmd + tablename + "(" + tableFieldStr + ")"
	Log.Debug("创建数据库表Cmd:", createTableCmd)
	CONN.Exec(createTableCmd)
}
