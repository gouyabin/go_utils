package utils

import (
	"encoding/json"
	"io"
)

func JsonDecoder(jsStr io.Reader) map[string]string {
	decoder := json.NewDecoder(jsStr)
	// fmt.Println("decoder", decoder)
	// 用于存放参数key=value数据
	var params map[string]string
	// 解析参数 存入map
	decoder.Decode(&params)
	return params
}
