package utils

import "testing"

func TestLogs(t *testing.T) {
	l := new(Logs)
	l.IsPrintFileUrl = false
	l.IsWriteFile = true
	l.Init()
	l.Debug("TestDebug")
	l.Info("TestInfo")
	l.Error("Error")
}
